2014-05-17  Bruce Korb  <bkorb@gnu.org>

	release version

2014-05-10  Bruce Korb  <bkorb@gnu.org>

	add delays to kill signals
	It seems that on some platforms, if signals arrive too quickly,
	the process winds up hanging instead of dying.  Therefore, in
	the shell die() function, wait a second between signals.

	* agen5/ag-text.def (SHELL_INIT_STR): wait one second after
	  the SIGTERM, SIGINT and SIGHUP signals.  Afterwards, send
	  a SIGKILL signal.

	* autoopts/mk-tpl-config.sh (fix_guile): this function runs
	  under "set -e", so do not fail-exit if grep does not find
	  the "noreturn" word.

2014-05-03  Bruce Korb  <bkorb@gnu.org>

	clean up "noreturn"
	This is a more rigorous fix for the libguile usage of the "noreturn"
	token.  It must *always* run, so it is folded into the
	mk-tpl-config.sh script.

	fix month

2014-04-19  Bruce Korb  <bkorb@gnu.org>

	final release tweaks

2014-04-13  Bruce Korb  <bkorb@gnu.org>

	ATTRIBUTE_FORMAT_ARG is now used
	The gettext() wrapper function needs to be markable as:
	  __attribute__((format_arg(1)))
	but since not all compilers accept it, we wrap it.

	Also, a new template for handling perl program options is added.

2014-03-29  Bruce Korb  <bkorb@gnu.org>

	argument to aoGetsText() is type "format"
	The aoGetsText() emitted i18n helper function needs its argument
	to have the "format_arg" attribute.

2014-03-23  Bruce Korb  <bkorb@gnu.org>

	Only apply texi2mdoc when it is needed.
	Several adjustments to tests, too.  Also, update cright updater flags.

2014-02-22  Bruce Korb  <bkorb@gnu.org>

	remove stray token

	update copyright update

2014-02-02  Bruce Korb  <bkorb@gnu.org>

	improve the man page

	polish copyright updating

2014-01-26  Bruce Korb  <bkorb@gnu.org>

	try to filter out "dash".

	add embedded options
	Some files may have different find-the-copyright-line expressions.
	Allow this by searching for --copyright-mark in the source file.
	That should stand out sufficiently.  Its use requires it be in
	some sort of comment.

2014-01-10  Bruce Korb  <bkorb@gnu.org>

	update news

	bump the libopts revision

	Add guards against inquery calls to option procedures
	The "help" printing code can call option handling code when the
	option arg has not been set.  Make sure the option arg handling
	code does not try to do stuff when there is no option argument.

	update copyrights
	update copyright dates.

2013-12-31  Bruce Korb  <bkorb@gnu.org>

	check for ptrdiff_t
	Guile defines this if HAVE_PTRDIFF_T is not defined, ergo add it
	to our config testing so as to not double define it in compilations.

	de-camelization and code cleaning

2013-12-21  Bruce Korb  <bkorb@gnu.org>

	adjust libopts.m4

2013-12-01  Bruce Korb  <bkorb@gnu.org>

	checkpoint status

2013-11-23  Bruce Korb  <bkorb@gnu.org>

	Do not abort from main()
	If you are going to create a core dump, keep the stack active
	and correct.

	* agen5/autogen.c: remove abend_env and abend_sig globals
	  (main): remove sigsetjmp call
	  (cleanup_and_abort): mark with _Noreturn
	  (ag_abend_at): likewise

2013-11-16  Bruce Korb  <bkorb@gnu.org>

	fix Guile headers
	Guile headers include a #define for noreturn:

	  #define SCM_NORETURN __attribute__ ((noreturn))

	which is a problem when other headers have a #define
	for "noreturn".

	* autoopts/fix-guile.sh: new script that fixes the Guile define
	  to replace "(noreturn)" with "(__noreturn)".
	* autoopts/bootstrap.dir: distribute the stinky thing
	* autoopts/Makefile.am: run the stinky thing when needed
	* config/misc.def: run the gl_STDNORETURN_H macro in configure
	* agen5/scribble.c : Warning patrol

2013-10-22  Bruce Korb  <bkorb@gnu.org>

	copyright notice address
	Change from Franklin Street to URL.
	Also update fdl.texi

2013-10-16  Bruce Korb  <bkorb@gnu.org>

	Version 5.18.2
	final polish

	LIBGUILE_PATH is not needed, but "tr" is
	* autoopts/test/defs.in: remove the code that uses LIBGUILE_PATH
	  and find the "tr" program
	* config/confmacs.tlib: remove the code that tried to create
	  LIBGUILE_PATH
	* compat/compat.h: #defined values used in preprocessing expressions
	  cannot be type cast

2013-10-12  Bruce Korb  <bkorb@gnu.org>

	fix directive derivations
	* doc/auto_gen-tpl.in: extract the directive information into files
	  that can be alphabetically sorted.
	* agen5/defDirect.c: all directives are now handled by specific
	  functions, even always-fail and always-ignored directives.
	* agen5/mk-stamps.sh: omit the #defines of ignored and handled-by-
	  error directive functions.
	* agen5/defLex.c(gather_name): correct skip-over-a-number code.
	* agen5/test/debug.test: use the new, improved "'...'" quotes
	  instead of the bad, old "``...''" quotes

2013-10-10  Bruce Korb  <bkorb@gnu.org>

	ChangeLog is now history

2013-10-06  Bruce Korb  <bkorb@gnu.org>

	quote CFLAGS value
	Thanks to Leo Davis --
	* autoopts/tpl/usage.tlib: The values passed through the environment must be
	  protected against shell tokenization.
	* doc/autogen-texi.txt: fix description of generated main() proc.
	* autoopts/tpl/getopt.tpl: declare optionUsage, too.
	* autoopts/tpl/optmain.tlib: clean up main proc generation
	* autoopts/test/getopt.test: adjust test

	From more email:
	* doc/auto_gen-tpl.in:  "directive" values are no longer extracted from
	  agen5/defDirect.c.  Instead, sed out the documentation.

2013-10-05  Bruce Korb  <bkorb@gnu.org>

	rework arg-optional
	The description in the doc was not as clear as possible.

2013-09-08  Bruce Korb  <bkorb@gnu.org>

	set release version

2013-09-05  Bruce Korb  <bkorb@gnu.org>

	allow alternate "no return" flags
	If "no-return" is specified, the default tag for the non-returning
	functions is "_Noreturn".  However, if the attribute is not the
	empty string, that will be used instead.  And doc the stuff.

2013-09-03  Bruce Korb  <bkorb@gnu.org>

	doc cleanup

2013-08-17  Bruce Korb  <bkorb@gnu.org>

	fix all non-flag values
	non-flag character option values will now start with 0x1001.
	The counter will be incremented with each option that does
	not have a flag character.

	* autoopts/tpl/optlib.tlib: define the counter and a function
	  that returns its incremented value.  Use that function
	  for emitting the "#define VALUE_OPT_..." preprocessing macro.
	* autoopts/tpl/opthead.tlib: likewise for the auto-supported
	  options ("help", et al.)
	* doc/auto-opts.tlib: the option count constraint is now
	  4,000 not 100.

2013-08-15  Bruce Korb  <bkorb@gnu.org>

	tpl: Ensure non-flag-char values are not chars
	* autoopts/tpl/opthead.tlib: set the option values to high values
	  when auto-supported options do not have associated flag chars.
	* autoopts/test/defs.in: ensure that nested invocations of
	  autogen add their trace output to the trace log file.
	* autoopts/test/getopt.test: reorganize so that the scripting is
	  easier to follow.
	* autoopts/tpl/usage.tlib: preserve the scratch directory when
	  VERBOSE is set to "true".

2013-08-11  Bruce Korb  <bkorb@gnu.org>

	templates: fix up FD number conflict
	* agen5/agShell.c (ag_scm_shellf): shell_cmd() *can* return NULL.
	* agen5/autogen.c (exit_cleanup): reset trace_fp to stderr after close
	* autoopts/tpl/str2mask.tpl: only scan over characters that are in
	  the list of mask names.
	* agen5/test/str2m.test: adjust test
	* autoopts/tpl/getopt.tpl: die if autogen exits non-zero
	* autoopts/tpl/usage.tlib: fd 8 is always used, so do not use it in
	  this template script.  Replace "8" with "7".

2013-07-28  Bruce Korb  <bkorb@gnu.org>

	fix typo

	Coverity tweaks

2013-07-27  Bruce Korb  <bkorb@gnu.org>

	update NEWS

	fix install version of str2init.tlib

2013-07-27  Bruce Korb  <bkorb@gnu.org>

	cast "char" to "unsigned char"
	* add-on/char-mapper/map-text.def:
	  The compiler will sign extend negatively valued characters
	  before casting to "unsigned", yielding largish values for
	  the 0xE2 character.

	Fix spelling of "precede".

2013-07-14  Bruce Korb  <bkorb@gnu.org>

	set release version

2013-07-12  Bruce Korb  <bkorb@gnu.org>

	bump version to 5.18

2013-07-11  Bruce Korb  <bkorb@gnu.org>

	handle flag options in embedded operands
	single character option "flags" were not correctly understood.
	Also, when looking for ambiguous option names, do not check
	the "doc" options -- their name strings are NULL.

	* autoopts/find.c (opt_ambiguities): do not compare ambiguous option name
	  against a NULL pointer (documentation name).

	* autoopts/load.c (optionMakePath): break path name canonicalization code
	  (optionLoadLine): save and restore processing flags and turn off
	  OPTPROC_ERRSTOP while processing a line.

	* autoopts/test/main.test: fix embedded flag option test.

	* autoopts/tpl/agtexi-cmd.tpl: if sedding the autogen name does not find
	  you the columns program name, then fall back to "which columns".
	* autoopts/tpl/cmd-doc.tlib: likewise
	* autoopts/tpl/def2pot.tpl: likewise
	* autoopts/tpl/getopt.tpl: likewise
	* autoopts/tpl/options.tpl: likewise
	* autoopts/tpl/str2init.tlib: likewise
	* autoopts/tpl/usage.tlib: likewise

2013-07-09  Bruce Korb  <bkorb@gnu.org>

	edit out testing stuff during install
	* autoopts/install-hook.sh: autoopts/tpl/options.tpl got dropped
	* compat/pathfind.c (pathfind): add a fallback path.

2013-07-07  Bruce Korb  <bkorb@gnu.org>

	replace more echoes with printfs
	Some echo commands like to process backslash escapes.
	* autoopts/tpl/mdoc2man.sh: fix the problem
	* autoopts/test/doc.test: adjust the test results
	* autoopts/test/keyword.test: likewise

2013-07-06  Bruce Korb  <bkorb@gnu.org>

	more doc tweaks

	improve "main shell-process" doc

2013-07-05  Bruce Korb  <bkorb@gnu.org>

	fix up fixup expression

2013-07-05  Bruce Korb  <bkorb@gnu.org>

	funnel all libopts exiting to option_exits
	Make certain that upon exit, stdout has an exit command
	whenever it is needed and not otherwise.

	Also, extract *all* global variable definitions to autoopts.h,
	guarded by "AUTOOPTS_INTERNAL".

2013-07-05  Bruce Korb  <bkorb@gnu.org>

	paged usage cleanup
	It, too, needs substantial work to cope with shell scripts reading
	standard input.

	* autoopts/pgusage.c (open_tmp_usage): extracted new proc
	  (mk_pager_cmd): likewise
	  (optionPagedUsage): remove some cluttered code and make sure
	  stray "exit 0" commands do not appear in the usage text and also
	  print usage text to stderr when necessary.

	* autoopts/ao-strs.def: new strings for that code.

2013-07-05  Bruce Korb  <bkorb@gnu.org>

	Mulligan: emit exit command for shell processing
	When libopts exits for any reason, the shell must be told to exit.

	* autoopts/proc-state.def: new option processing state: shell-output
	  marks the options as requiring shell status to stdout and all
	  other text to stderr.
	* autoopts/test/defs.in: remove from usage any line starting with
	  "exit" and then a digit.
	* autoopts/test/getopt.test: specify shell-process type main().
	* autoopts/test/shell.test: likewise, and adjust usage text
	* autoopts/tpl/optcode.tlib: add new SHELL_OUTPUT process flag
	* autoopts/tpl/optlib.tlib: main proc attribute validation
	* autoopts/tpl/optmain.tlib: do not fiddle stdout.  Use new flag.
	* autoopts/tpl/usage.tlib: use unspecified main procedure type.
	* autoopts/usage.c (optionUsage): print exit code in presence of
	  OPTPROC_SHELL_OUTPUT flag, and then redirect output to stderr.
	* */opts.def: eliminame NLS code.  Nothing is translatable.
	* doc/autogen-intro.texi: belated cleanup

2013-07-04  Bruce Korb  <bkorb@gnu.org>

	use test guards only for test-main
	Inserting these guards are a dinkleberry from a decade ago.
	Insert the guards IFF "test-main" is specified in the
	option definition file.  If "main" is specified, then
	the code is unconditionally compiled in.

	* autoopts/tpl/optmain.tlib:
	  "guarded-test-main" is a boolean computed in optlib.tlib.
	  emit the test main guards IFF this is #t (true).
	* autoopts/tpl/optcode.tlib:
	  Do not emit a define for the test main guard.
	* autoopts/test/argument.test: adjust
	* doc/auto-opts.tlib: remove debug code

2013-07-03  Bruce Korb  <bkorb@gnu.org>

	optionPutShell must write usage to stderr
	The recommended usage for option parsing programs that emit text
	to be evaluated by a shell must always write usage text to stderr.

	* agen5/autogen.c (done_check): fix warning
	* autoopts/test/defs.in
	  send xtrace output to fd 8 and capture stderr when getting help.
	  optionPutShell() programs now emit usage to stderr.

	* autoopts/tpl/optcode.tlib: unistd now required for optionPutShell
	* autoopts/tpl/optlib.tlib: rework main proc variables
	* autoopts/tpl/optmain.tlib: use these variables to decide how to
	  produce the doUsageOption code.

	* doc/auto-opts.tlib: improve debug aids
	* doc/mk-agen-texi.sh: emit all the log files on error

	* autoopts/test/handler.test: adjustments
	* autoopts/test/immediate.test: likewise
	* autoopts/test/shell.test: likewise
	* autoopts/test/stdopts.test: likewise

2013-07-02  Bruce Korb  <bkorb@gnu.org>

	allow suffixes on exe name
	* autoopts/tpl/agtexi-cmd.tpl:
	  The templates must use the columns program from the same directory
	  as autogen itself.  Use echo/sed to replace "autogen" with "columns".
	* autoopts/tpl/agtexi-file.tpl: likewise
	* autoopts/tpl/cmd-doc.tlib: likewise
	* autoopts/tpl/def2pot.tpl: likewise
	* autoopts/tpl/options.tpl: likewise
	* autoopts/tpl/str2init.tlib: likewise
	* autoopts/tpl/usage.tlib: likewise

	* agen5/opts.def: separate autoopts options from autogen ones.
	* autoopts/test/vendor.test: adjust test
	* config/libopts.def: shorten comment

2013-06-29  Bruce Korb  <bkorb@gnu.org>

	out-of-memory handling
	* autoopts/tpl/opthead.tlib: capture the exit code named "no-mem" or "nomem".
	  (nomem_err): emit new function when "die-code" is specified and a no-mem
	  exit code is also specified.
	* doc/autogen-texi.txt: document this.

2013-05-31  Bruce Korb  <bkorb@gnu.org>

	handle Solaris
	* agen5/ag-text.def (EMIT_INVAL_PORT): format used by long value
	  (ULONG_FMT): it is unsigned, not signed.  Rename.
	  (RESOLVE_SCM_NUMBER): use "ULONG_FMT" instead
	* agen5/funcDef.c (build_defs): use ULONG_FMT instead
	* agen5/funcEval.c (scm2display): use ULONG_FMT instead
	* templates and scripts everywhere: cope with Solaris' "tr" program
	  and use 'printf' when "\f" sequences need to be emitted literally.

2013-05-18  Bruce Korb  <bkorb@gnu.org>

	increase timeout for usage.test

	set release version to 5.17.4

	clean here string
	* agen5/defLex.c (build_here_str): allow backslash to escape any horizontal
	  white space character, not just a tab.
	* autoopts/tpl/cmd-doc.tlib (mk-description): blank lines within a description
	  need to cause a blank line on output.  Replace any residual empty lines
	  with ".sp".
